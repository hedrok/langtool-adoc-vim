#!/usr/bin/env python3

# :set errorformat=LT\ '%f':%l:%v:\ %m
# LT 028.adoc:{t['line']}:{t['column']}: {t['message']} - {t['ruleid']}-{t['underscored']}\n{t['fulltext']}\n
    #print(f"LT 028.adoc:{t['line']}:{t['column']}: {t['ruleid']}-{t['underscored']} - {t['message']}{suggestionstr}\n{t['fulltext']}\n")

import argparse
import re
import subprocess
import sys
import tempfile

debug = False
if debug:
    def debug(*pargs, **kargs):
        print(*pargs, **kargs)
else:
    def debug(*pargs, **kargs):
        pass

# So, what do we need?

# 1. Stripping/parsing asciidoc syntax
# 2. Getting rules:
# 2.1 Rule can be global or local
# 2.2 Rule can have string or regex
# 2.3 Local rules have line number limits

def parseLanguageToolOutput(text):
    """
    Parses output of LanguageTool
    to dicts with keys

    num, line, column, ruleid, message, suggestion, underscored, fulltext

    num - number of error
    fulltext - original full text of error
    underscored - underscored (with ^ symbols) text
    suggestion is none if omited

    Other fields are taken directly from error. For the following error

    1.) Line 7, column 17, Rule ID: MORFOLOGIK_RULE_UK_UA
    Message: Знайдено потенційну орфографічну помилку.
    Suggestion: Ролі; Коліна; Колін; Коліно; Поліна
    ... go Rowling will go Rowling. Якщо яка-небудь Ролінґ може трапитися, вона обовʼязково трапиться. ...
                                                    ^^^^^^                                             

    keys will have values:
    {
        'num': 1,
        'line': 7,
        'column': 17,
        'ruleid': 'MORFOLOGIK_RULE_UK_UA',
        'message': 'Знайдено потенційну орфографічну помилку.',
        'suggestion': 'Ролі; Коліна; Колін; Коліно; Поліна',
        'underscored': 'Ролінґ',
        'sentence': '...',
        'fulltext': '...',
    }

    'fulltext' is omitted for brevity.

    First debug messages and last message about time are ignored.

    @return list of such dicts
    """

    # remove all debug info till "1.) "
    i = text.find("\n1.) ")
    j = text.find("\nTime: ")
    text = text[i+1:j]

    expr = r'(?P<num>[0-9]+).\) Line (?P<line>[0-9]+), column (?P<column>[0-9]+), Rule ID: (?P<ruleid>.+)\nMessage: (?P<message>.+)\n(Suggestion: (?P<suggestion>.*)\n)?(?P<sentence>.*)\n(?P<spacebefore> *)(?P<underscores>\^+)'
    expr = re.compile(expr)

    errors = text.split("\n\n")
    ret = []
    for er in errors:
        res = re.match(expr, er)
        if not res:
            print(f"Couldn't parse error. Error text:\n===\n{er}\n===")
            sys.exit(1)
        erdict = res.groupdict()
        erdict['fulltext'] = er
        ruleid = erdict['ruleid']
        ruleid = ruleid.replace(' premium: false', '').replace(' premium: true', '')
        ruleid = re.sub(' prio=[-0-9]+', '', ruleid)
        erdict['ruleid'] = ruleid
        sbl = len(erdict['spacebefore'])
        unl = len(erdict['underscores'])
        erdict['underscored'] = erdict['sentence'][sbl:sbl+unl]
        del erdict['underscores']
        del erdict['spacebefore']
        for k in ('num', 'line', 'column'):
            erdict[k] = int(erdict[k])
        erdict['rule'] = f"{erdict['ruleid']}-{erdict['underscored']}"
        ret.append(erdict)

    return ret

class AdocInput:
    """
    Class that prepares adoc text for languagetool processing
    and then converts columns to proper values
    """

    def __init__(self, inputfile):
        """
        @param inputfile - opened for reading input file
        
        Sets inputtext attribute to text without adoc syntax
        """
        filterRE = r'@langtool(?P<regex>-regex)?(?P<global>-global)? (?P<rule>.*)$'
        filterRE = re.compile(filterRE)

        includeRE = r'@langtool-include (?P<filename>.*)$'

        asciidocReplace = {
            '__': '',
            '**': '',
            'footnote:[': ' (',
            ']': ')',
            '= ': '',
            '== ': '',
            '=== ': '',
            '==== ': '',
            '"`': '«',
            '`"': '»',
            '"\'': '„',
            '\'"': '”',
            "'`": '„',
            "`'": '”',
            '--': '—',
            # Specific, move to config
            '{stress}': '',
        }

        replacesInLine = [{}]
        text = ''

        allFilters = []
        curFilters = []
        includeFiles = []

        def getLines():
            yield from inputfile
            while includeFiles:
                filename = includeFiles.pop(0)
                debug(f"Continuing with lines from {filename=}")
                with open(filename) as f:
                    yield from f

        # For each line we need to know:
        for line in getLines():
            lineRepl = {}
            debug(f"Line {line}")
            lineNum = len(replacesInLine)
            for (k, v) in asciidocReplace.items():
                i = -1
                debug(f"\tSearch for {k}...")
                while True:
                    i = line.find(k, i + 1)
                    if i == -1:
                        break
                    debug(f"\tFound index {i}...")
                    lineRepl[i] = len(k) - len(v)
                    debug(f"\tAdding for line {len(replacesInLine)} replacement {i} of len {lineRepl[i]}")
            lineRepl = dict(sorted(lineRepl.items()))
            replacesInLine.append(lineRepl)
            filterMatch = re.search(filterRE, line)
            if filterMatch:
                debug(f"Match in line {line}, dict: {filterMatch.groupdict()}")
                g = filterMatch.groupdict()
                filt = {
                    'rule': g['rule'],
                    'lineBegin': lineNum,
                    'regex': g['regex'],
                    'global': g['global'],
                }
                if g['global']:
                    allFilters.append(filt)
                else:
                    curFilters.append(filt)
            if line.strip() == '' and curFilters:
                for filt in curFilters:
                    filt['lineEnd'] = lineNum
                allFilters.extend(curFilters)
                curFilters.clear()

            includeMatch = re.search(includeRE, line)
            if includeMatch:
                debug(f"Include match in line {line}, dict: {includeMatch.groupdict()}")
                filename = includeMatch.groupdict()['filename']
                includeFiles.append(filename)

            # remove comments:
            i = line.find('//')
            if i != -1:
                line = line[:i] + '\n'
            text += line

        if curFilters:
            for filt in curFilters:
                filt['lineEnd'] = lineNum
            allFilters.extend(curFilters)
            curFilters.clear()

        debug(replacesInLine)
        debug("Filters:")
        debug(allFilters)

        for (k, v) in asciidocReplace.items():
            text = text.replace(k, v)

        self.inputtext = text
        self.replacesInLine = replacesInLine
        self.allFilters = allFilters

    def filterErrors(self, ltErrors):
        """
        Filters errors by tags:
        @langtool @langtool-global @langtool-regex @langtool-regex-global
        """
        # awful, but will work... There is a lot room to optimize
        def testError(er):
            debug(f"Work on error {er['rule']}")
            for filt in self.allFilters:
                if not filt['global']:
                    if filt['lineBegin'] > er['line']:
                        continue
                    if filt['lineEnd'] < er['line']:
                        continue
                if not filt['regex']:
                    if filt['rule'] == er['rule']:
                        debug(f"\t+Filter rule {filt['rule']} does match error rule {er['rule']}")
                        return False
                    else:
                        debug(f"\t-Filter rule {filt['rule']} doesn't match error rule {er['rule']}")
                else: # regex
                    if re.fullmatch(filt['rule'], er['rule']):
                        debug(f"+Filter regex rule {filt['rule']} does match error rule {er['rule']}")
                        return False
                    else:
                        debug(f"\t-Filter regex rule {filt['rule']} doesn't match error rule {er['rule']}")
            debug("\t-not filtered")
            return True

        return list(filter(testError, ltErrors))

    def updateColumns(self, ltErrors):
        """
        Accepts list returned by parseLanguageToolOutput
        Updates column where needed
        """
        for er in ltErrors:
            erInd = er['column'] - 1
            debug("Before:")
            debug(self.replacesInLine[er['line']])
            for (ind, val) in self.replacesInLine[er['line']].items():
                if erInd >= ind:
                    erInd += val
            if er['column'] - 1 != erInd:
                debug(f"Yup, changed! In line {er['line']} from {er['column']} to {erInd +1}")
            er['column'] = erInd + 1

parser = argparse.ArgumentParser(description='Prefilter file, call languagetool, postfilter errors')
parser.add_argument('filename', help='file to check')
args = parser.parse_args()
filename = args.filename

with open(filename) as adocfile:
    adoc = AdocInput(adocfile)

with tempfile.NamedTemporaryFile(mode='w+') as tmp:
    tmp.write(adoc.inputtext)
    tmp.flush()
    langtoolresult = subprocess.run(
        [
            'languagetool-commandline',
            '-l',
            'uk',
            tmp.name
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

rc = langtoolresult.returncode
output = langtoolresult.stdout.decode('utf-8')
error = langtoolresult.stderr.decode('utf-8')

if langtoolresult.returncode:
    print(f"Error with langtool, return code: {rc}, stderr:\n{error}\stdout:\n{output}")
    sys.exit(1)

debug(f"Languagetool output: {output=}")

objects = parseLanguageToolOutput(output)
lenBeforeFilter = len(objects)
objects = adoc.filterErrors(objects)
lenAfterFilter = len(objects)
adoc.updateColumns(objects)

if not objects:
    print("It seems there are no LanguageTool errors... Weird... Congratulations!")
    sys.exit(0)

print(f"Number of LanguageToolErrors: {lenAfterFilter}, filtered: {lenBeforeFilter-lenAfterFilter}")
print("All rules:")
rules = set(er['rule'] for er in objects)
for rule in rules:
    print(f"\t{rule}")

for t in objects:
    suggestionstr = f" (suggestions: {t['suggestion']})" if t['suggestion'] else ''
    print(f"LT '{filename}':{t['line']}:{t['column']}: {t['rule']} - {t['message']}{suggestionstr}\n{t['fulltext']}\n")
